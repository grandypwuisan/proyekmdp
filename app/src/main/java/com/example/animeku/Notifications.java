package com.example.animeku;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "notifs")
public class Notifications {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "idnotif")
    private int idnotif;

    @ColumnInfo(name = "idfav")
    private int idfav;

    @ColumnInfo(name = "animetitle")
    private String animetitle;

    @ColumnInfo(name = "namauser")
    private String namauser;

    @ColumnInfo(name = "emailuser")
    private String emailuser;

    @ColumnInfo(name = "date")
    private String date;

    public Notifications(int idfav, String animetitle, String namauser, String emailuser, String date) {
        this.idfav = idfav;
        this.animetitle = animetitle;
        this.namauser = namauser;
        this.emailuser = emailuser;
        this.date = date;
    }

    public int getIdnotif() {
        return idnotif;
    }

    public void setIdnotif(int idnotif) {
        this.idnotif = idnotif;
    }

    public int getIdfav() {
        return idfav;
    }

    public void setIdfav(int idfav) {
        this.idfav = idfav;
    }

    public String getAnimetitle() {
        return animetitle;
    }

    public void setAnimetitle(String animetitle) {
        this.animetitle = animetitle;
    }

    public String getNamauser() {
        return namauser;
    }

    public void setNamauser(String namauser) {
        this.namauser = namauser;
    }

    public String getEmailuser() {
        return emailuser;
    }

    public void setEmailuser(String emailuser) {
        this.emailuser = emailuser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
