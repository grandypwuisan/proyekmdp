package com.example.animeku;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ReviewActivity extends AppCompatActivity {
    int rate=0, id_anime, id_update;
    String nama;
    Boolean terisi;
    user userlogin;
    ratingandreviews a;

    ImageView iv1, iv2, iv3, iv4, iv5;
    Button btnComplete;
    EditText etReview;

    dvRateView db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        Intent intent = getIntent();
        if(intent.hasExtra("rate")){
            rate = Integer.parseInt(intent.getStringExtra("rate"));
        }
        if(intent.hasExtra("EXTRA_OBJEKUSERLOGIN")){
            userlogin = intent.getParcelableExtra("EXTRA_OBJEKUSERLOGIN");
            nama = userlogin.getNama();
        }
        if(intent.hasExtra("id_anime")){
            id_anime = Integer.parseInt(intent.getStringExtra("id_anime"));
        }

        db=dvRateView.getdvRateView(ReviewActivity.this);

        btnComplete = findViewById(R.id.button2);
        etReview = findViewById(R.id.editText);
        iv1 = findViewById(R.id.imageView2);
        iv2 = findViewById(R.id.imageView3);
        iv3 = findViewById(R.id.imageView4);
        iv4 = findViewById(R.id.imageView5);
        iv5 = findViewById(R.id.imageView6);
        iv1.setImageResource(R.drawable.star_empty);
        iv2.setImageResource(R.drawable.star_empty);
        iv3.setImageResource(R.drawable.star_empty);
        iv4.setImageResource(R.drawable.star_empty);
        iv5.setImageResource(R.drawable.star_empty);

        new LoadRateViewTask1().execute();
        setRate();

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate=1;
                setRate();
            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate=2;
                setRate();
            }
        });
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate=3;
                setRate();
            }
        });
        iv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate=4;
                setRate();
            }
        });
        iv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate=5;
                setRate();
            }
        });
        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rate==0){
                    Toast.makeText(ReviewActivity.this, "You Must Rate to Complete", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(terisi){
                        ratingandreviews m=new ratingandreviews(nama, id_anime, rate, etReview.getText().toString());

                        new UpdateReviewTask().execute(m);
                    }
                    else{
                        ratingandreviews m=new ratingandreviews(nama, id_anime, rate, etReview.getText().toString());

                        new AddReviewTask().execute(m);
                    }
                }
            }
        });
    }

    private void setRate(){
        if(rate==1){
            iv1.setImageResource(R.drawable.star_full);
            iv2.setImageResource(R.drawable.star_empty);
            iv3.setImageResource(R.drawable.star_empty);
            iv4.setImageResource(R.drawable.star_empty);
            iv5.setImageResource(R.drawable.star_empty);
        }
        else if(rate==2){
            iv1.setImageResource(R.drawable.star_full);
            iv2.setImageResource(R.drawable.star_full);
            iv3.setImageResource(R.drawable.star_empty);
            iv4.setImageResource(R.drawable.star_empty);
            iv5.setImageResource(R.drawable.star_empty);
        }
        else if(rate==3){
            iv1.setImageResource(R.drawable.star_full);
            iv2.setImageResource(R.drawable.star_full);
            iv3.setImageResource(R.drawable.star_full);
            iv4.setImageResource(R.drawable.star_empty);
            iv5.setImageResource(R.drawable.star_empty);
        }
        else if(rate==4){
            iv1.setImageResource(R.drawable.star_full);
            iv2.setImageResource(R.drawable.star_full);
            iv3.setImageResource(R.drawable.star_full);
            iv4.setImageResource(R.drawable.star_full);
            iv5.setImageResource(R.drawable.star_empty);
        }
        else if(rate==5){
            iv1.setImageResource(R.drawable.star_full);
            iv2.setImageResource(R.drawable.star_full);
            iv3.setImageResource(R.drawable.star_full);
            iv4.setImageResource(R.drawable.star_full);
            iv5.setImageResource(R.drawable.star_full);
        }
    }

    private class AddReviewTask extends AsyncTask<ratingandreviews, Void, Void> {

        @Override
        protected Void doInBackground(ratingandreviews... ratingandreviews) {
            db.rateviewDAO().insertRateView(ratingandreviews[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);
            Toast.makeText(ReviewActivity.this, "Review Complete", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public class LoadRateViewTask1 extends AsyncTask<Void, Void, List<ratingandreviews>> {

        @Override
        protected List<ratingandreviews> doInBackground(Void... voids) {
            return db.rateviewDAO().getRateView(id_anime);
        }

        @Override
        protected void onPostExecute(List<ratingandreviews> m) {
            super.onPostExecute(m);
            terisi=false;
            for (int i = 0; i < m.size(); i++) {
                if(m.get(i).getEmail().equals(nama)){
                    if(m.get(i).getId_anime()==id_anime){
                        terisi=true;
                        a=m.get(i);
                        rate= (int) m.get(i).getRating();
                        setRate();
                        etReview.setText(m.get(i).getReview());
                    }
                }
            }
        }
    }

    private class UpdateReviewTask extends AsyncTask<ratingandreviews, Void, Void> {

        @Override
        protected Void doInBackground(ratingandreviews... ratingandreviews) {
            db.rateviewDAO().delete(a);
            db.rateviewDAO().insertRateView(ratingandreviews[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);
            Toast.makeText(ReviewActivity.this, "Review Complete", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
