package com.example.animeku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class favoritAdapter extends RecyclerView.Adapter<favoritAdapter.animeViewHolder> {

    private static RVClicklistener mylistener;
    private List<anime> animeArrayList;
    private int posisi;

    public favoritAdapter(List<anime> animeArrayList, RVClicklistener rvcl) {
        this.animeArrayList = animeArrayList;
        mylistener = rvcl;
    }
    //disni kemungkinan yang bikin eror


    @NonNull
    @Override
    public animeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inf = LayoutInflater.from(parent.getContext());
        View v = inf.inflate(R.layout.layout_rv_favoritanime,parent,false);
        return new animeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull animeViewHolder holder, int position) {

        new DownloadImageTask(holder.imgview).execute(animeArrayList.get(position).getImage_url());
        holder.txtJudul.setText(animeArrayList.get(position).getTitle());
        holder.txtEps.setText(animeArrayList.get(position).getType()+", Episodes : "+animeArrayList.get(position).getEpisodes());
        holder.txtScore.setText("MAL Score : "+animeArrayList.get(position).getRating());

    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(animeArrayList != null)size = animeArrayList.size();
        return size;
    }

    public class animeViewHolder extends RecyclerView.ViewHolder {
        ImageView imgview;
        TextView txtJudul,txtEps,txtScore;
        public animeViewHolder(@NonNull View itemView) {
            super(itemView);
            imgview = itemView.findViewById(R.id.fav_img_view);
            txtJudul = itemView.findViewById(R.id.fav_judulAnime);
            txtEps = itemView.findViewById(R.id.fav_JenisEps);
            txtScore = itemView.findViewById(R.id.fav_ScoreMal);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mylistener.recyclerViewListClick(v,animeViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
