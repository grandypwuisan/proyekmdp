package com.example.animeku;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface favoritDAO {
    @Insert
    void insertFavorit (Favorit favorit);

    @Delete
    void deleteFav (Favorit favorit);

    @Query("SELECT * FROM favorit WHERE email LIKE :email")
    List<Favorit> getAllFav(String email);

    @Query("SELECT * FROM favorit WHERE email LIKE :email and idAnime = :id")
    List<Favorit>  favorit(String email, int id);

    @Query("SELECT * FROM favorit ")
    List<Favorit> getAll();

    @Query("SELECT MAX(idFavorit) from favorit")
    int getMax();
}
