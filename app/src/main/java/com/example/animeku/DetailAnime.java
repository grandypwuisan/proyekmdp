package com.example.animeku;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DetailAnime extends AppCompatActivity {

    int int_rate1, int_rate2, int_rate3, int_rate4, int_rate5, max_fav = 0;
    ArrayList<ratingandreviews> list = new ArrayList<>();
    user userlogin;
    anime anime;
    TextView tvJudul, tvScore, tvEpisodes, tvStatus, tvRating, tvAired, tvReviewAll, tvToReview, tvMyReview, tvMyReview2, tvRatePeop, tvAllRate, tvTest, tvMyNameReview;
    ImageButton btnFav;
    WebView vd, img;
    ImageView rate1, rate2, rate3, rate4, rate5, imgPP;
    RecyclerView rv;
    charadapter adapter;
    ArrayList<Characters> listchar = new ArrayList<>();
    ProgressBar pgBar, pbRate1, pbRate2, pbRate3, pbRate4, pbRate5;
    List<Favorit> tempFav = new ArrayList<>();
    dbFavorit db;
    dvRateView dv;
    dbNotif dbNotif;
    String emailUser = "willy";
    LinearLayout llRateAnime, llWriteReview;
    Notifications deletedNotif;
    Favorit deletedFav;
    boolean reviewed = false;
    boolean like = false;
    boolean isClickable = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_anime);
        btnFav = findViewById(R.id.btnFav);

        userlogin = getIntent().getParcelableExtra("EXTRA_OBJEKUSERLOGIN");
        anime = getIntent().getParcelableExtra("anime");
        emailUser = getIntent().getStringExtra("email");
        dv=dvRateView.getdvRateView(DetailAnime.this);
        db = Room.databaseBuilder(getApplicationContext(),dbFavorit.class,"FavDB").build();
        dbNotif = Room.databaseBuilder(getApplicationContext(),dbNotif.class,"NotifDB").build();
        ambilFavAwal();

        tvEpisodes = findViewById(R.id.tvEpisodes);
        tvJudul = findViewById(R.id.tvJudul);
        tvRating = findViewById(R.id.tvRating);
        tvScore = findViewById(R.id.tvScore);
        tvStatus = findViewById(R.id.tvStatus);
        tvAired = findViewById(R.id.tvAired);
        tvReviewAll = findViewById(R.id.tvReviewAll);
        tvToReview = findViewById(R.id.tvToReview);
        tvTest = findViewById(R.id.textView7);

        vd = findViewById(R.id.videoView);
        img = findViewById(R.id.imageView);
        rv = findViewById(R.id.rvChar);
        pgBar = findViewById(R.id.progressBar);

        img.loadData("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='"+anime.getImage_url()+"'/></body></html>" ,"text/html",  "UTF-8");
        //Toast.makeText(this, anime.getTrailer_url(), Toast.LENGTH_SHORT).show();
        vd.getSettings().setJavaScriptEnabled(true);
        vd.getSettings().setPluginState(WebSettings.PluginState.ON);
        vd.loadUrl(anime.getTrailer_url());
        vd.setWebChromeClient(new WebChromeClient());

        tvStatus.setText(anime.getStatus());
        tvScore.setText("Score: "+anime.getScore());
        tvRating.setText(anime.getRating());
        tvJudul.setText(anime.getTitle());
        tvEpisodes.setText(anime.getEpisodes()+"("+anime.getType()+")");
        String aired_date = "";
        for (int i = 0; i < 3; i++) {
            if(i==2){
                aired_date += anime.getDate()[i];
            }
            else{
                aired_date += anime.getDate()[i]+"/";
            }
        }
        tvAired.setText("Aired: "+aired_date);

        if(!tvStatus.getText().toString().equals("Not yet aired")){
            llRateAnime=findViewById(R.id.rateAnime);
            getLayoutInflater().inflate(R.layout.layout_rateanime, llRateAnime);
            new LoadIsReviewed().execute();
            tvAllRate=findViewById(R.id.tvAllRate);
            tvRatePeop=findViewById(R.id.tvRatePeop);
            pbRate1=findViewById(R.id.progressBar6);
            pbRate2=findViewById(R.id.progressBar7);
            pbRate3=findViewById(R.id.progressBar8);
            pbRate4=findViewById(R.id.progressBar9);
            pbRate5=findViewById(R.id.progressBar10);
            new LoadRateAnimeTask().execute();
        }

        tvReviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, RateViewActivity.class);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                startActivity(intent);
            }
        });
        tvToReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, RateViewActivity.class);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                startActivity(intent);
            }
        });

        adapter = new charadapter(listchar);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv.setAdapter(adapter);

        pgBar.setVisibility(View.VISIBLE);
        final RequestQueue requestQueue = Volley.newRequestQueue(DetailAnime.this);
        final String url = "https://api.jikan.moe/v3/anime/"+anime.getMal_id()+"/characters_staff";
        StringRequest strReg = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray list = jsonObject.getJSONArray("characters");
                    for (int i = 0; i < 10; i++) {
                        final JSONObject characters = list.getJSONObject(i);
                        JSONArray va = characters.getJSONArray("voice_actors");
                        JSONObject objva = va.getJSONObject(0);
                        if(objva != null){
                            final String url = "https://api.jikan.moe/v3/person/"+objva.getInt("mal_id");
                            StringRequest strReg = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        final Characters charandva = new Characters(characters, jsonObject);
                                        listchar.add(charandva);
                                        adapter.notifyDataSetChanged();
                                    } catch (Exception e) {
                                        //Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });

                            requestQueue.add(strReg);
                        }
                    }
                    pgBar.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    pgBar.setVisibility(View.INVISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(strReg);
    }
    public void ambilFavAwal(){new getFavAwal().execute();}

    private class getFavAwal extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            tempFav.clear();
            tempFav.addAll(db.favoritDAO().favorit(emailUser,anime.getMal_id()));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(tempFav.size() == 1){
                likeAnime();
            }
            else{
                dislikeAnime();
            }
            isClickable = true;
        }
    }
    private class insertFavorit extends AsyncTask<Favorit,Void,Void>{

        @Override
        protected Void doInBackground(Favorit... favorits) {
            db.favoritDAO().insertFavorit(favorits[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            likeAnime();
            showToast("Ditambahkan ke Favoritku",R.drawable.toastberhasil);
            isClickable = true;
            new getMaxFav().execute();
        }
    }

    private class deleteFav extends AsyncTask<Favorit,Void,Void>{

        @Override
        protected Void doInBackground(Favorit... favorits) {
            deletedFav = favorits[0];
            db.favoritDAO().deleteFav(favorits[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dislikeAnime();
            showToast("Dihilangkan dari Favoritku",R.drawable.toast);
            tempFav.clear();
            isClickable = true;
            new getNotif().execute(deletedFav.getIdFavorite());
        }
    }
    public void favoritkanAnime(){
        isClickable = false;
        Favorit f = new Favorit(emailUser,anime.getMal_id());
        tempFav.add(f);
        new insertFavorit().execute(f);
    }

    public void hilangkanFavorit(){
        isClickable = false;
        new deleteFav().execute(tempFav.get(0));
    }

    public void favoritekan(View v){
        if(isClickable){
            if(!like) {
                favoritkanAnime();
            }
            else{
                //ini kalau mau unfavorite
                hilangkanFavorit();
            }
        }

    }

    public void likeAnime(){
        //ini kalo mau favoritekan
        btnFav.setImageResource(R.drawable.ic_favorite);
        like = true;
    }
    public void dislikeAnime(){
        btnFav.setImageResource(R.drawable.ic_favorite_border);
        like = false;
    }
    public void showToast(String text,int image) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));

        TextView toastText = layout.findViewById(R.id.toast_text);
        ImageView toastImage = layout.findViewById(R.id.toast_image);

        toastText.setText(text);
        toastImage.setImageResource(image);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflate = getMenuInflater();
        inflate.inflate(R.menu.option_menu_rating, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.opthome){
            Intent rateint = new Intent(DetailAnime.this, MainActivity.class);
            rateint.putExtra("EXTRA_OBJEKUSERLOGIN", userlogin);
            startActivity(rateint);
        }
        return true;
    }

    private void ReviewRate(){
        tvMyReview = findViewById(R.id.tvReviewIt);
        rate1=findViewById(R.id.ivRate2);
        rate2=findViewById(R.id.ivRate3);
        rate3=findViewById(R.id.ivRate4);
        rate4=findViewById(R.id.ivRate5);
        rate5=findViewById(R.id.ivRate6);
        rate1.setImageResource(R.drawable.star_empty);
        rate2.setImageResource(R.drawable.star_empty);
        rate3.setImageResource(R.drawable.star_empty);
        rate4.setImageResource(R.drawable.star_empty);
        rate5.setImageResource(R.drawable.star_empty);

        tvMyReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                startActivity(intent);
            }
        });
        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                intent.putExtra("rate", "1");
                startActivity(intent);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                intent.putExtra("rate", "2");
                startActivity(intent);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                intent.putExtra("rate", "3");
                startActivity(intent);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                intent.putExtra("rate", "4");
                startActivity(intent);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                intent.putExtra("rate", "5");
                startActivity(intent);
            }
        });
    }

    private void alreadyReviewRate(int rating){
        imgPP=findViewById(R.id.ivPP);
        rate1=findViewById(R.id.ivMyRate1);
        rate2=findViewById(R.id.ivMyRate2);
        rate3=findViewById(R.id.ivMyRate3);
        rate4=findViewById(R.id.ivMyRate4);
        rate5=findViewById(R.id.ivMyRate5);
        tvMyReview=findViewById(R.id.tvMyReview);
        tvMyReview2=findViewById(R.id.tvMyReview2);
        tvMyNameReview=findViewById(R.id.tvMyNameReview);
        imgPP.setImageResource(R.drawable.ic_people_black_24dp);
        if(rating==1){
            rate1.setImageResource(R.drawable.star_full);
            rate2.setImageResource(R.drawable.star_empty);
            rate3.setImageResource(R.drawable.star_empty);
            rate4.setImageResource(R.drawable.star_empty);
            rate5.setImageResource(R.drawable.star_empty);
        }
        else if(rating==2){
            rate1.setImageResource(R.drawable.star_full);
            rate2.setImageResource(R.drawable.star_full);
            rate3.setImageResource(R.drawable.star_empty);
            rate4.setImageResource(R.drawable.star_empty);
            rate5.setImageResource(R.drawable.star_empty);
        }
        else if(rating==3){
            rate1.setImageResource(R.drawable.star_full);
            rate2.setImageResource(R.drawable.star_full);
            rate3.setImageResource(R.drawable.star_full);
            rate4.setImageResource(R.drawable.star_empty);
            rate5.setImageResource(R.drawable.star_empty);
        }
        else if(rating==4){
            rate1.setImageResource(R.drawable.star_full);
            rate2.setImageResource(R.drawable.star_full);
            rate3.setImageResource(R.drawable.star_full);
            rate4.setImageResource(R.drawable.star_full);
            rate5.setImageResource(R.drawable.star_empty);
        }
        else if(rating==5){
            rate1.setImageResource(R.drawable.star_full);
            rate2.setImageResource(R.drawable.star_full);
            rate3.setImageResource(R.drawable.star_full);
            rate4.setImageResource(R.drawable.star_full);
            rate5.setImageResource(R.drawable.star_full);
        }
        tvMyReview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DetailAnime.this, ReviewActivity.class);
                intent.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                intent.putExtra("id_anime", anime.getMal_id()+"");
                intent.putExtra("email", emailUser);
                startActivity(intent);
            }
        });
    }

    public class LoadRateAnimeTask extends AsyncTask<Void, Void, List<ratingandreviews>> {

        @Override
        protected List<ratingandreviews> doInBackground(Void... voids) {
            return dv.rateviewDAO().getRateView(anime.getMal_id());
        }

        @Override
        protected void onPostExecute(List<ratingandreviews> m) {
            super.onPostExecute(m);
            int_rate1 = 0;
            int_rate2 = 0;
            int_rate3 = 0;
            int_rate4 = 0;
            int_rate5 = 0;
            for (int i = 0; i < m.size(); i++) {
                if (m.get(i).getRating() == 1) {
                    int_rate1++;
                } else if (m.get(i).getRating() == 2) {
                    int_rate2++;
                } else if (m.get(i).getRating() == 3) {
                    int_rate3++;
                } else if (m.get(i).getRating() == 4) {
                    int_rate4++;
                } else if (m.get(i).getRating() == 5) {
                    int_rate5++;
                }
            }
            pbRate1.setMax(m.size());
            pbRate2.setMax(m.size());
            pbRate3.setMax(m.size());
            pbRate4.setMax(m.size());
            pbRate5.setMax(m.size());
            pbRate1.setProgress(int_rate5);
            pbRate2.setProgress(int_rate4);
            pbRate3.setProgress(int_rate3);
            pbRate4.setProgress(int_rate2);
            pbRate5.setProgress(int_rate1);
            double a, b, c, d, e;
            if (m.size() == 0) {
                tvRatePeop.setText(m.size() + "");
                tvAllRate.setText(m.size() + "");
            } else {
                a = 1 * (int_rate1 / m.size());
                b = 2 * (int_rate2 / m.size());
                c = 3 * (int_rate3 / m.size());
                d = 4 * (int_rate4 / m.size());
                e = 5 * (int_rate5 / m.size());
                tvRatePeop.setText(m.size() + "");
                tvAllRate.setText((a + b + c + d + e) + "");
            }
        }
    }

    public class LoadIsReviewed extends AsyncTask<Void, Void, List<ratingandreviews>> {

        @Override
        protected List<ratingandreviews> doInBackground(Void... voids) {
            return dv.rateviewDAO().getRateView(anime.getMal_id());
        }

        @Override
        protected void onPostExecute(List<ratingandreviews> m) {
            super.onPostExecute(m);
            reviewed=false;
            int pos=0;
            for (int i = 0; i < m.size(); i++) {
                if(m.get(i).getEmail().equals(userlogin.getNama())){
                    reviewed=true;
                    pos=i;
                }
            }
            if(reviewed){
                tvTest.setText("Your Review");
                llWriteReview=findViewById(R.id.rateMyView);
                getLayoutInflater().inflate(R.layout.layout_alreadyreviewed, llWriteReview);
                alreadyReviewRate((int) m.get(pos).getRating());
                tvMyNameReview.setText(m.get(pos).getEmail());
                tvMyReview.setText(m.get(pos).getReview());
            }
            else{
                tvTest.setText("Rate Your Anime");
                llWriteReview=findViewById(R.id.rateMyView);
                getLayoutInflater().inflate(R.layout.layout_ratemyanime, llWriteReview);
                ReviewRate();
            }
        }
    }

    private class InsertNotif extends AsyncTask<Notifications,Void,Void> {

        @Override
        protected Void doInBackground(Notifications... users) {
            dbNotif.notifDAO().insertNotif(users[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

    private class getNotif extends AsyncTask<Integer,Void,Void> {

        @Override
        protected Void doInBackground(Integer... users) {
            if(dbNotif.notifDAO().getNotif(users[0]).size() > 0){
                deletedNotif = dbNotif.notifDAO().getNotif(users[0]).get(0);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(deletedNotif != null){
                new DeleteNotif().execute(deletedNotif);
            }
        }
    }

    private class DeleteNotif extends AsyncTask<Notifications,Void,Void> {

        @Override
        protected Void doInBackground(Notifications... users) {
            dbNotif.notifDAO().delete(users[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

    private class getMaxFav extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            max_fav = db.favoritDAO().getMax();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            String aired_date = "";
            for (int i = 0; i < 3; i++) {
                if(i==2){
                    if(anime.getDate()[i]<10){
                        aired_date += "0"+anime.getDate()[i];
                    }
                    else{
                        aired_date += anime.getDate()[i];
                    }
                }
                else{
                    if(anime.getDate()[i]<10){
                        aired_date += "0"+anime.getDate()[i]+"/";
                    }
                    else{
                        aired_date += anime.getDate()[i]+"/";
                    }
                }
            }
            Notifications notif = new Notifications(max_fav, anime.getTitle(), userlogin.getNama(), userlogin.getEmail(), aired_date);
            new InsertNotif().execute(notif);
        }

    }
}
