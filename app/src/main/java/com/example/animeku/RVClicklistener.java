package com.example.animeku;

import android.view.View;

public interface RVClicklistener {
    public void recyclerViewListClick(View v, int pos);
}
