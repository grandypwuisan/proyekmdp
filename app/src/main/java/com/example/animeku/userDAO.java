package com.example.animeku;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface userDAO {
    @Query("SELECT * FROM user where email LIKE :email")
    List<user> getUser(String email);

    @Query("SELECT * FROM user where email LIKE :email and password LIKE :pass")
    List<user> cekLogin(String email,String pass);

    @Insert
    void insertuser(user barang);

    @Update
    void update(user barang);

    @Delete
    void delete(user barang);
}
