package com.example.animeku;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationMenu;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
public class logregActivity extends AppCompatActivity {

    ArrayList<Notifications> listnotif = new ArrayList<>();
    ArrayList<user> listUser = new ArrayList<>();
    dbUser db;
    dbNotif dbNotif;
    String tuser,tnama,tpass,temail;
    BottomNavigationView bottomNavigationView;
    int RC_SIGN_IN;
    dbFavorit dbFav;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logreg);

        //registrasi channel notif
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Name";
            String description = "Desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("Channel1", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        gantiFragment(new loginFragment());
        db = Room.databaseBuilder(getApplicationContext(),dbUser.class,"UserDB").build();
        dbFav = Room.databaseBuilder(getApplicationContext(),dbFavorit.class,"FavDB").build();
        dbNotif = Room.databaseBuilder(getApplicationContext(),dbNotif.class,"NotifDB").build();
        bottomNavigationView = findViewById(R.id.bn_main);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId()==R.id.menu_login){
                    gantiFragment(new loginFragment());
                }
                else{
                    gantiFragment(new RegisterFragment());
                }
                return true;
            }
        });
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if(account != null){
            //jika sudah ada account yg terpasang, sign out dulu
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            GoogleSignInClient client = GoogleSignIn.getClient(this, gso);
            client.signOut();
        }

        new RegisterAllNotif().execute();
    }
    public void showToast(String text,int image) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));

        TextView toastText = layout.findViewById(R.id.toast_text);
        ImageView toastImage = layout.findViewById(R.id.toast_image);

        toastText.setText(text);
        toastImage.setImageResource(image);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);

        toast.show();
    }

    private void gantiFragment(Fragment f){
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container,f).commit();
    }


    public void register(String user, String nama, String pass, String email){
        tuser = user;
        tnama = nama;
        tpass = pass;
        temail = email;
        new register().execute();
    }

    public void login(String email,String pass){
        tpass = pass;
        temail = email;
        new Login().execute();
    }

    public void googlesignin(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = client.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Intent mainint = new Intent(logregActivity.this, MainActivity.class);
            mainint.putExtra("EXTRA_OBJEKUSERLOGIN",new user(account.getDisplayName(), "", account.getGivenName(), account.getEmail()));
            startActivity(mainint);
        } catch (ApiException e) {
            Toast.makeText(this, e.getStatusCode()+"", Toast.LENGTH_SHORT).show();
        }
    }

    private class register extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            listUser.clear();
            listUser.addAll(db.userDAO().getUser(temail));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(listUser.size()>0) {
                String username, email2, pass2;
               // Toast.makeText(getApplicationContext(),"User Sudah Ada",Toast.LENGTH_SHORT).show();
                showToast("User Sudah Ada",R.drawable.toast);
            }
            else{
                //disini bikin user ------------------------------------------------
                user u = new user(tuser,tpass,tnama,temail);
                new InsertUser().execute(u);
            }
        }
    }

    private class InsertUser extends AsyncTask<user,Void,Void> {

        @Override
        protected Void doInBackground(user... users) {
            db.userDAO().insertuser(users[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(),"Berhasil Register",Toast.LENGTH_SHORT).show();
            showToast("Berhasil Register",R.drawable.toastberhasil);
        }

    }

    private class Login extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            listUser.clear();
            listUser.addAll(db.userDAO().cekLogin(temail,tpass));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(listUser.size()>0){
                Intent pindah = new Intent(logregActivity.this,MainActivity.class);
                pindah.putExtra("EXTRA_USERLOGIN",temail);
                pindah.putExtra("EXTRA_OBJEKUSERLOGIN",listUser.get(0));
                startActivity(pindah);
            }
            else{
                //Toast.makeText(getApplicationContext(),"GAGAL LOGIN",Toast.LENGTH_SHORT).show();
                showToast("Gagal Login",R.drawable.toast);
            }
        }
    }

    private class RegisterAllNotif extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            listnotif.clear();
            listnotif.addAll(dbNotif.notifDAO().getAllNotif());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (int i = 0; i < listnotif.size(); i++) {
                //register allnotif
                Calendar calendar = Calendar.getInstance();
                Intent intent = new Intent(getApplicationContext(), Receiver.class);
                intent.putExtra("idFav", listnotif.get(i).getIdfav());
                intent.putExtra("animetitle", listnotif.get(i).getAnimetitle());
                intent.putExtra("namauser", listnotif.get(i).getNamauser());
                intent.putExtra("emailuser", listnotif.get(i).getEmailuser());
                intent.putExtra("date", listnotif.get(i).getDate());
                sendBroadcast(intent);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pendingIntent);
            }
        }
    }
}
