package com.example.animeku;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RateViewActivity extends AppCompatActivity {
    RecyclerView rv;
    ArrayList<ratingandreviews> list =new ArrayList<>();

    dvRateView db;
    RateViewAdapter adapter;
    int id_anime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_view);

        Intent intent=getIntent();
        if(intent.hasExtra("id_anime")){
            id_anime=Integer.parseInt(intent.getStringExtra("id_anime"));
        }

        rv=findViewById(R.id.rv_rateview);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(RateViewActivity.this));

        db=dvRateView.getdvRateView(RateViewActivity.this);
        adapter=new RateViewAdapter(list);
        rv.setAdapter(adapter);

        new LoadRateViewTask().execute();

        adapter.setOnClickedCallback(new RateViewAdapter.OnClickedCallback() {
            @Override
            public void onClick(ratingandreviews m) {

            }
        });
    }
    public class LoadRateViewTask extends AsyncTask<Void, Void, List<ratingandreviews>> {

        @Override
        protected List<ratingandreviews> doInBackground(Void... voids) {
            return db.rateviewDAO().getRateView(id_anime);
        }

        @Override
        protected void onPostExecute(List<ratingandreviews> m) {
            super.onPostExecute(m);
            list.clear();
            for (int i = 0; i < m.size(); i++) {
                if(m.get(i).getReview().equals("")){
                    m.remove(i);
                    i--;
                }
            }
            list.addAll(m);
            adapter.notifyDataSetChanged();
        }
    }
}
