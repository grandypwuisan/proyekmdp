package com.example.animeku;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Query;

import java.util.List;

@Entity(tableName = "ratingandreviews")
public class ratingandreviews {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_review")
    private int id_review;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "id_anime")
    private int id_anime;

    @ColumnInfo(name = "rating")
    private double rating;

    @ColumnInfo(name = "review")
    private String review;

    public ratingandreviews(String email, int id_anime, double rating, String review) {
        this.email = email;
        this.id_anime = id_anime;
        this.rating = rating;
        this.review = review;
    }

    public int getId_review() {
        return id_review;
    }

    public void setId_review(int id_review) {
        this.id_review = id_review;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId_anime() {
        return id_anime;
    }

    public void setId_anime(int id_anime) {
        this.id_anime = id_anime;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

}
