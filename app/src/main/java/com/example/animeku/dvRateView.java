package com.example.animeku;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {ratingandreviews.class}, version = 2, exportSchema = false)
public abstract class dvRateView extends RoomDatabase {
    public abstract rateviewDAO rateviewDAO();
    public static dvRateView INSTANCE;

    public static dvRateView getdvRateView(Context context){
        if(INSTANCE==null) {
            INSTANCE = Room.databaseBuilder(
                    context,
                    dvRateView.class,
                    "RateandReviewAnimekuDB")
                    .build();
        }
        return INSTANCE;
    }
}
