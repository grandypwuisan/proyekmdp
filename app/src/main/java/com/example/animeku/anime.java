package com.example.animeku;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class anime implements Parcelable{
    private int mal_id;
    private String image_url, title, type, status;
    private int[] date = {0, 0, 0};
    private int episodes;
    private String trailer_url, rating;
    private double score;

    public anime(JSONObject object){
        try {
            this.mal_id = object.getInt("mal_id");
            this.image_url = object.getString("image_url");
            this.title = object.getString("title");
            this.type = object.getString("type");
            this.status = object.getString("status");

            //format tanggal menjadi dd/mm/yyyy
            JSONObject obj = object.getJSONObject("aired");
            JSONObject prop = obj.getJSONObject("prop");
            JSONObject from = prop.getJSONObject("from");
            if(from.getInt("day") > 0){
                date[0] = from.getInt("day");
            }
            if(from.getInt("month") > 0){
                date[1] = from.getInt("month");
            }
            if(from.getInt("year") > 0){
                date[2] = from.getInt("year");
            }

            this.episodes = object.getInt("episodes");
            this.trailer_url = object.getString("trailer_url");
            this.rating = object.getString("rating");
            this.score = object.getDouble("score");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected anime(Parcel in) {
        mal_id = in.readInt();
        image_url = in.readString();
        title = in.readString();
        type = in.readString();
        status = in.readString();
        date = in.createIntArray();
        episodes = in.readInt();
        trailer_url = in.readString();
        rating = in.readString();
        score = in.readDouble();
    }

    public static final Creator<anime> CREATOR = new Creator<anime>() {
        @Override
        public anime createFromParcel(Parcel in) {
            return new anime(in);
        }

        @Override
        public anime[] newArray(int size) {
            return new anime[size];
        }
    };

    public int getMal_id() {
        return mal_id;
    }

    public void setMal_id(int mal_id) {
        this.mal_id = mal_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getepisodes() {
        return episodes;
    }

    public void setepisodes(int episodes) {
        this.episodes = episodes;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public int getEpisodes() {
        return episodes;
    }

    public void setEpisodes(int episodes) {
        this.episodes = episodes;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int[] getDate() {
        return date;
    }

    public void setDate(int[] date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "anime{" +
                "mal_id=" + mal_id +
                ", title='" + title + '\'' +
                ", image_url='" + image_url + '\'' +
                ", score=" + score +
                ", type='" + type + '\'' +
                ", episodes=" + episodes +
                ", trailer_url='" + trailer_url + '\'' +
                ", rating='" + rating + '\'' +
                ", status='" + status + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mal_id);
        parcel.writeString(image_url);
        parcel.writeString(title);
        parcel.writeString(type);
        parcel.writeString(status);
        parcel.writeIntArray(date);
        parcel.writeInt(episodes);
        parcel.writeString(trailer_url);
        parcel.writeString(rating);
        parcel.writeDouble(score);
    }
}
