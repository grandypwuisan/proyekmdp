package com.example.animeku;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {user.class},version=3, exportSchema = false)
public abstract class dbUser extends RoomDatabase {
    public abstract userDAO userDAO();
}
