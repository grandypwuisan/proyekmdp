package com.example.animeku;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NotifDAO {
    @Query("SELECT * FROM notifs")
    List<Notifications> getAllNotif();

    @Query("SELECT * FROM notifs where idfav LIKE :id")
    List<Notifications> getNotif(int id);

    @Insert
    void insertNotif(Notifications notif);

    @Update
    void update(Notifications notif);

    @Delete
    void delete(Notifications notif);
}
