package com.example.animeku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class seasonAdapter extends RecyclerView.Adapter<seasonAdapter.seasonViewHolder> {
    private static RVClicklistener mylistener;
    private List<anime> animeArrayList;
    private int posisi;

    public seasonAdapter(List<anime> animeArrayList, RVClicklistener rvcl) {
        this.animeArrayList = animeArrayList;
        mylistener = rvcl;
    }

    @NonNull
    @Override
    public seasonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inf = LayoutInflater.from(parent.getContext());
        View v = inf.inflate(R.layout.layout_rv_kecil,parent,false);
        return new seasonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull seasonViewHolder holder, int position) {
        new DownloadImageTask(holder.imgview).execute(animeArrayList.get(position).getImage_url());
        holder.txtJudul.setText(animeArrayList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(animeArrayList != null)size = animeArrayList.size();
        return size;
    }

    public class seasonViewHolder extends RecyclerView.ViewHolder {
        ImageView imgview;
        TextView txtJudul;
        public seasonViewHolder(@NonNull View itemView) {
            super(itemView);
            imgview = itemView.findViewById(R.id.season_img_view);
            txtJudul = itemView.findViewById(R.id.season_judulAnime);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mylistener.recyclerViewListClick(v,seasonViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
