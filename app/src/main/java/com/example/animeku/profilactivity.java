package com.example.animeku;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import androidx.room.Update;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class profilactivity extends AppCompatActivity {

    user userlogin;
    dbUser db;
    EditText nama, pass,cpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilactivity);
        //ambil objek userlogin
        userlogin = getIntent().getParcelableExtra("EXTRA_OBJEKUSERLOGIN");
        db = Room.databaseBuilder(getApplicationContext(),dbUser.class,"UserDB").build();
        nama = findViewById(R.id.edNamaEdit);
        pass = findViewById(R.id.edPassEdit);
        cpass = findViewById(R.id.edCPassEdit);
        nama.setText(userlogin.getNama());
    }

    public void gantiNama(View v){
        userlogin.setNama(nama.getText().toString());
        new UpdateUser().execute(userlogin);
    }

    public void gantiPassword(View v){
        if(pass.getText().toString().equals(cpass.getText().toString())){
            userlogin.setPassword(pass.getText().toString());
            new UpdateUser().execute(userlogin);
        }
        else{
            showToast("Confirm Password Tidak Sesuai",R.drawable.toast);
        }
    }

    private class UpdateUser extends AsyncTask<user,Void,Void> {

        @Override
        protected Void doInBackground(user... barangs) {
            db.userDAO().update(barangs[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            showToast("Berhasil Update",R.drawable.toastberhasil);
        }

    }
    public void showToast(String text,int image) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));

        TextView toastText = layout.findViewById(R.id.toast_text);
        ImageView toastImage = layout.findViewById(R.id.toast_image);

        toastText.setText(text);
        toastImage.setImageResource(image);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);

        toast.show();
    }
}
