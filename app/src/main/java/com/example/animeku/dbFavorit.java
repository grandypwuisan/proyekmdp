package com.example.animeku;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Favorit.class},version = 2,exportSchema = false)
public abstract class dbFavorit extends RoomDatabase {
    public abstract favoritDAO favoritDAO();
}
