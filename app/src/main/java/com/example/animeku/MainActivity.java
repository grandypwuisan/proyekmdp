package com.example.animeku;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ProgressBar pgBar, pgBarSeason;
    ArrayList<anime> listanime = new ArrayList<>();
    List<anime> listSeason = new ArrayList<>();
    user userlogin;
    ImageView[] arrimg;
    seasonAdapter adapter;
    animeadapter adapterairing;
    RecyclerView rv, rvairing;
    EditText tahun;
    Spinner musim;
    RequestQueue[] requestQueue = new RequestQueue[4];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int temp = 2;
        //ambil objek userlogin
        userlogin = getIntent().getParcelableExtra("EXTRA_OBJEKUSERLOGIN");

        //background
        ConstraintLayout constraintLayout = findViewById(R.id.container);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();
        rv = findViewById(R.id.rvSeason);
        rvairing = findViewById(R.id.rvAiring);
        rvairing.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapterairing = new animeadapter(listanime, new RVClicklistener() {
            @Override
            public void recyclerViewListClick(View v, int pos) {
                Intent anint = new Intent(MainActivity.this, DetailAnime.class);
                anint.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                anint.putExtra("anime", listanime.get(pos));
                anint.putExtra("email",userlogin.getEmail());
                startActivity(anint);
            }
        });
        rvairing.setAdapter(adapterairing);
        tahun = findViewById(R.id.editTextTahun);
        musim = findViewById(R.id.spinner);
        pgBar = findViewById(R.id.progressBar4);
        pgBarSeason = findViewById(R.id.progressBar2);

        pgBar.setVisibility(View.VISIBLE);
        for (int i = 0; i < 4; i++) {
            requestQueue[i] = Volley.newRequestQueue(MainActivity.this);
        }
        final String url = "https://api.jikan.moe/v3/top/anime/1/airing";
        StringRequest strReg = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    listanime.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    final JSONArray list = jsonObject.getJSONArray("top");
                    int urutananime = -1;
                    while(true){
                        urutananime+=1;
                        JSONObject anime = list.getJSONObject(urutananime);
                            //mulai dari sini untuk anime id
                            final String url = "https://api.jikan.moe/v3/anime/"+anime.getInt("mal_id");
                            StringRequest strReg = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        final anime item = new anime(jsonObject);
                                        if(item.getDate()[0] > 0 && listanime.size() < 10 && item.getTrailer_url() != null){
                                            listanime.add(item);
                                        }
                                        adapterairing.notifyDataSetChanged();
                                    } catch (Exception e) {
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                            if(listanime.size() == 10){
                                break;
                            }
                            requestQueue[0].add(strReg);
                    }
                    pgBar.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    pgBar.setVisibility(View.INVISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue[1].add(strReg);
        loadSeason("winter",2020);
    }

    public void loadSeason(String season,int year){
        pgBar.setVisibility(View.VISIBLE);
        listSeason.clear();
        final String url = "https://api.jikan.moe/v3/season/"+year+"/"+season;
        //Toast.makeText(MainActivity.this,url,Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    final JSONObject jsonObject = new JSONObject(response);
                    final JSONArray arr = jsonObject.getJSONArray("anime");
                    JSONObject obj = arr.getJSONObject(0);
                    int ctr = 0;
                    while(true){
                        final String urlBaru = "https://api.jikan.moe/v3/anime/"+arr.getJSONObject(ctr).getString("mal_id");
                        StringRequest request = new StringRequest(StringRequest.Method.GET, urlBaru, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject1 = new JSONObject(response);
                                    anime tempAnime = new anime(jsonObject1);
                                    if(tempAnime.getDate()[0] > 0 && listSeason.size() <= 19){
                                        listSeason.add(tempAnime);
                                    }
                                    if(listSeason.size() == 19) {
                                        final RVClicklistener rvClicklistener = new RVClicklistener() {
                                            @Override
                                            public void recyclerViewListClick(View v, int pos) {
                                                //Toast.makeText(MainActivity.this,listSeason.get(pos).getTitle(),Toast.LENGTH_SHORT).show();
                                                Intent anint = new Intent(MainActivity.this, DetailAnime.class);
                                                anint.putExtra("EXTRA_OBJEKUSERLOGIN", userlogin);
                                                anint.putExtra("anime", listSeason.get(pos));
                                                anint.putExtra("email", userlogin.getEmail());
                                                startActivity(anint);
                                            }
                                        };
                                        adapter = new seasonAdapter(listSeason, rvClicklistener);
                                        RecyclerView.LayoutManager lm = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
                                        rv.setLayoutManager(lm);
                                        rv.setAdapter(adapter);
                                        //Toast.makeText(MainActivity.this,"Sudah",Toast.LENGTH_LONG).show();
                                    }
                                }catch (Exception e){

                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        ctr++;
                        if(listSeason.size() == 19){
                            break;
                        }
                        requestQueue[2].add(request);
                    }
                    pgBarSeason.setVisibility(View.INVISIBLE);
                }
                catch (Exception e){
                    pgBarSeason.setVisibility(View.INVISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue[3].add(stringRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu2,menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_logout:
                Intent pindah = new Intent(MainActivity.this,logregActivity.class);
                startActivity(pindah);
                break;
            case R.id.menu_profil:
                Intent pindah2 = new Intent(MainActivity.this,profilactivity.class);
                pindah2.putExtra("EXTRA_OBJEKUSERLOGIN",userlogin);
                startActivity(pindah2);
                break;
            case R.id.keFavoritku:
                Intent keFavAct = new Intent(MainActivity.this, favActivity.class);
                keFavAct.putExtra("email", userlogin.getEmail());
                startActivity(keFavAct);
                break;
        }
        return true;
    }

    public void gantiSeason(View v){
        int year = Integer.parseInt(tahun.getText().toString());
        String season = musim.getSelectedItem().toString();
        loadSeason(season,year);
    }
}


