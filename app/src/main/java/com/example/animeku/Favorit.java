package com.example.animeku;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "favorit")
public class Favorit {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idFavorit")
    private int idFavorite;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "idAnime")
    private int idAnime;

    public Favorit(String email, int idAnime) {
        this.email = email;
        this.idAnime = idAnime;
    }

    public int getIdFavorite() {
        return idFavorite;
    }

    public String getEmail() {
        return email;
    }

    public int getIdAnime() {
        return idAnime;
    }

    public void setIdFavorite(int idFavorite) {
        this.idFavorite = idFavorite;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIdAnime(int idAnime) {
        this.idAnime = idAnime;
    }
}
