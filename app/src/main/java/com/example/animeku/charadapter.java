package com.example.animeku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class charadapter extends RecyclerView.Adapter<charadapter.ViewHolder> {
    ArrayList<Characters> listchar;

    public charadapter(ArrayList<Characters> listchar) {
        this.listchar = listchar;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_char, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        new DownloadImageTask(holder.ivChar).execute(listchar.get(position).getImage_url());
        new DownloadImageTask(holder.ivVA).execute(listchar.get(position).getImage_va());
        holder.tvChar.setText(listchar.get(position).getName());
        holder.tvVA.setText(listchar.get(position).getName_va());
    }

    @Override
    public int getItemCount() {
        return listchar.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivChar, ivVA;
        TextView tvChar, tvVA;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivChar = itemView.findViewById(R.id.ivChar);
            tvChar = itemView.findViewById(R.id.textView2);
            ivVA = itemView.findViewById(R.id.ivVA);
            tvVA = itemView.findViewById(R.id.textView3);
        }
    }
}
