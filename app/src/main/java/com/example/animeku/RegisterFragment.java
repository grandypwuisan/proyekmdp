package com.example.animeku;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {



    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    EditText user,email,pass;
    Button reg;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        user = view.findViewById(R.id.edUsernameReg);
        email = view.findViewById(R.id.edEmailReg);
        pass = view.findViewById(R.id.edPasswordReg);
        reg = view.findViewById(R.id.btnRegister);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!email.getText().toString().equals("") && !user.getText().toString().equals("") && !pass.getText().toString().equals("")) {
                    logregActivity parent = (logregActivity) getActivity();
                    parent.register(user.getText().toString(), user.getText().toString(), pass.getText().toString(), email.getText().toString());
                    bersihkanField();
                }
                else{
                    //Toast.makeText(getActivity(),"Pastikan semua field terisi",Toast.LENGTH_SHORT).show();
                    logregActivity parent = (logregActivity) getActivity();
                    parent.showToast("Pastikan semua field terisi",R.drawable.toastfieldkosong);
                }
            }
        });
    }

    public void bersihkanField(){
        email.setText("");
        pass.setText("");
        user.setText("");
    }

}
