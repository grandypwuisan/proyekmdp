package com.example.animeku;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class favActivity extends AppCompatActivity {

    String emailUser = "";
    List<Favorit> favoritList = new ArrayList<>();
    RecyclerView rv;
    favoritAdapter adapter;
    dbFavorit db;
    List<anime>animeList = new ArrayList<>();
    StringRequest stringRequest;
    ProgressBar pgb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);
        //background
        LinearLayout constraintLayout = findViewById(R.id.containerfav);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();
        emailUser = getIntent().getStringExtra("email");
        db = Room.databaseBuilder(getApplicationContext(),dbFavorit.class,"FavDB").build();
        rv = findViewById(R.id.fav_RV);
        pgb = findViewById(R.id.progressBar3);
        isiFav();
    }

    public void isiFav(){
        new getAllFav().execute();
    }
    public void tambahArr(anime item)
    {

    }
    private class getAllFav extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            favoritList.clear();
            favoritList.addAll(db.favoritDAO().getAllFav(emailUser));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(favoritList.isEmpty()){
                pgb.setVisibility(View.GONE);
                showToast("Kamu Tidak Suka Anime yaaaa?",R.drawable.toastfieldkosong);
            }
            else{
                animeList.clear();
                int ctr = 0;
                while(ctr < favoritList.size())
                {
                    final String url = "https://api.jikan.moe/v3/anime/" + favoritList.get(ctr).getIdAnime();
                    stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //listOfResponse.add(response);
                            try {
                                //callback.onSuccessResponse(response);
                                JSONObject jsonObject = new JSONObject(response);
                                final anime item = new anime(jsonObject);
                                animeList.add(item);

                                if(animeList.size() == favoritList.size())
                                {
                                    //Toast.makeText(favActivity.this, "Jumlah Fav : "+animeList.size(),Toast.LENGTH_LONG).show();
                                    final RVClicklistener rvcl = new RVClicklistener() {
                                        @Override
                                        public void recyclerViewListClick(View v, int pos) {
                                            Intent anint = new Intent(favActivity.this, DetailAnime.class);
                                            anint.putExtra("anime", animeList.get(pos));
                                            anint.putExtra("email",emailUser);
                                            startActivity(anint);
                                        }
                                    };
                                    adapter = new favoritAdapter(animeList, rvcl);
                                    RecyclerView.LayoutManager lm = new LinearLayoutManager(favActivity.this,LinearLayoutManager.HORIZONTAL,false);
                                    rv.setLayoutManager(lm);
                                    rv.setAdapter(adapter);
                                    //disini hilangkan pgbar
                                    pgb.setVisibility(View.GONE);
                                }
                            }
                            catch (Exception e){
                                pgb.setVisibility(View.INVISIBLE);
                                Toast.makeText(favActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                    ctr++;
                    RequestQueue requestQueue = Volley.newRequestQueue(favActivity.this);//butuh ini untuk lanjutkan iterasi
                    requestQueue.add(stringRequest);
                }
            }

        }
        public void showToast(String text,int image) {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));

            TextView toastText = layout.findViewById(R.id.toast_text);
            ImageView toastImage = layout.findViewById(R.id.toast_image);

            toastText.setText(text);
            toastImage.setImageResource(image);

            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);

            toast.show();
        }
    }
}
