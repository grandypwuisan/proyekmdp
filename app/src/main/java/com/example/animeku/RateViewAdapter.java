package com.example.animeku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RateViewAdapter extends RecyclerView.Adapter<RateViewAdapter.RateViewViewHolder> {
    private ArrayList<ratingandreviews> list;

    private OnClickedCallback onClickedCallback;
    public void setOnClickedCallback(OnClickedCallback onClickedCallback){
        this.onClickedCallback=onClickedCallback;
    }

    public RateViewAdapter(ArrayList<ratingandreviews> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RateViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rateview, parent, false);
        RateViewViewHolder holder=new RateViewViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RateViewViewHolder holder, int position) {
        ratingandreviews m=list.get(position);
        holder.bind(m);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RateViewViewHolder extends RecyclerView.ViewHolder{

        ImageView img, rate1, rate2, rate3, rate4, rate5;
        TextView txtReview, txtNama;
        public RateViewViewHolder(@NonNull View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.profilepic_rateview);
            txtNama=itemView.findViewById(R.id.name_rateview);
            txtReview=itemView.findViewById(R.id.review_rateview);
            rate1=itemView.findViewById(R.id.rate1_rateview);
            rate2=itemView.findViewById(R.id.rate2_rateview);
            rate3=itemView.findViewById(R.id.rate3_rateview);
            rate4=itemView.findViewById(R.id.rate4_rateview);
            rate5=itemView.findViewById(R.id.rate5_rateview);
        }

        void bind(final ratingandreviews m){
            img.setImageResource(R.drawable.ic_people_black_24dp);
            txtReview.setText(m.getReview());
            txtNama.setText(m.getEmail());
            rate2.setImageResource(R.drawable.star_empty);
            rate3.setImageResource(R.drawable.star_empty);
            rate4.setImageResource(R.drawable.star_empty);
            rate5.setImageResource(R.drawable.star_empty);
            rate1.setImageResource(R.drawable.star_full);
            if(m.getRating()>=2){
                rate2.setImageResource(R.drawable.star_full);
                if(m.getRating()>=3){
                    rate3.setImageResource(R.drawable.star_full);
                    if(m.getRating()>=4){
                        rate4.setImageResource(R.drawable.star_full);
                        if(m.getRating()>=5){
                            rate5.setImageResource(R.drawable.star_full);
                        }
                    }
                }
            }
        }
    }

    public interface OnClickedCallback{
        void onClick(ratingandreviews m);
    }
}
