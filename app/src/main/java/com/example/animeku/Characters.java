package com.example.animeku;

import org.json.JSONException;
import org.json.JSONObject;

public class Characters {
    private String name, name_va, image_url, image_va;

    public Characters(JSONObject object, JSONObject objva) {
        try {
            this.name = object.getString("name");
            this.image_url = object.getString("image_url");
            this.name_va = objva.getString("name");
            this.image_va = objva.getString("image_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getName_va() {
        return name_va;
    }

    public void setName_va(String name_va) {
        this.name_va = name_va;
    }

    public String getImage_va() {
        return image_va;
    }

    public void setImage_va(String image_va) {
        this.image_va = image_va;
    }
}
