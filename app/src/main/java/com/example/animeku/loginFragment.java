package com.example.animeku;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class loginFragment extends Fragment {


    public loginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    EditText email,pass;
    Button login;
    SignInButton signInButton;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email = view.findViewById(R.id.edUsernameLogin);
        pass = view.findViewById(R.id.edPasswordLogin);
        login = view.findViewById(R.id.btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!email.getText().toString().equals("") && !pass.getText().toString().equals("")) {
                    logregActivity parent = (logregActivity) getActivity();
                    parent.login(email.getText().toString(),pass.getText().toString());
                }
                else{
                    //Toast.makeText(getActivity(),"Pastikan semua field terisi",Toast.LENGTH_SHORT).show();
                    logregActivity parent = (logregActivity) getActivity();
                    parent.showToast("Pastikan semua field terisi",R.drawable.toastfieldkosong);
                }
            }
        });
        signInButton = view.findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logregActivity parent = (logregActivity) getActivity();
                parent.googlesignin();
            }
        });
    }
}
