package com.example.animeku;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Receiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        int id_fav = arg1.getIntExtra("idFav", 0);
        String animetitle = arg1.getStringExtra("animetitle");
        String namauser = arg1.getStringExtra("namauser");
        String emailuser = arg1.getStringExtra("emailuser");
        int[] date = {1, 1, 2000};
        int[] spaces = {-1, -1, -1};
        String aired_date = arg1.getStringExtra("date");
        for (int i = 0; i < 3; i++) {
            if(i!= 0){
                spaces[i] = aired_date.indexOf("/", spaces[i-1]+1);
            }
            else{
                spaces[i] = aired_date.indexOf("/", 0);
            }
        }
        date[0] = Integer.parseInt(aired_date.substring(0, spaces[0]));
        date[1] = Integer.parseInt(aired_date.substring(spaces[0]+1, spaces[1]));
        date[2] = Integer.parseInt(aired_date.substring(spaces[1]+1, aired_date.length()));

        Date todate = Calendar.getInstance().getTime();
        DateFormat today = new SimpleDateFormat("dd/MM/yyyy");
        if(today.format(todate).equals(aired_date)){
            Intent intent = new Intent(arg0, favActivity.class);
            intent.putExtra("email", emailuser);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(arg0, 0, intent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(arg0, "Channel1")
                    .setSmallIcon(R.drawable.animeku_logo)
                    .setContentTitle("Your anime is now playing!")
                    .setContentText(namauser+", your favorite anime, "+animetitle+" is out now")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(arg0);
            notificationManager.notify(id_fav, builder.build());
        }
    }
}
