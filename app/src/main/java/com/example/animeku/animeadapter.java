package com.example.animeku;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class animeadapter extends RecyclerView.Adapter<animeadapter.ViewHolder> {
    private static RVClicklistener mylistener;
    private ArrayList<anime> listanime;
    private int posisi;

    public animeadapter(ArrayList<anime> listanime, RVClicklistener rvcl) {
        this.listanime = listanime;
        mylistener = rvcl;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inf = LayoutInflater.from(parent.getContext());
        View v = inf.inflate(R.layout.item_anime,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        new DownloadImageTask(holder.imgview).execute(listanime.get(position).getImage_url());
        holder.txtJudul.setText(listanime.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return listanime.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgview;
        TextView txtJudul;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgview = itemView.findViewById(R.id.animeImg);
            txtJudul = itemView.findViewById(R.id.animeJudul);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mylistener.recyclerViewListClick(v,ViewHolder.this.getLayoutPosition());
                }
            });
        }
    }
}
