package com.example.animeku;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Notifications.class},version = 2,exportSchema = false)
public abstract class dbNotif extends RoomDatabase {
    public abstract NotifDAO notifDAO();
}
