package com.example.animeku;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface rateviewDAO {
    @Query("SELECT * FROM ratingandreviews where id_anime = :id_anime")
    List<ratingandreviews> getRateView(int id_anime);

    @Insert
    void insertRateView(ratingandreviews rateview);

    @Update
    void update(ratingandreviews rateview);

    @Delete
    void delete(ratingandreviews rateview);
}
